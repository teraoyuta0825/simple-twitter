package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		String id = request.getParameter("edit");
		Message message = null;
		List<String> errorMessages = new ArrayList<>();

		if (!StringUtils.isBlank(id) && id.matches("^[1-9][0-9]*")) {
			int messagesId = Integer.parseInt(id);
			message = new MessageService().select(messagesId);
		}

		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		String text = request.getParameter("text");
		String id = request.getParameter("id");
		int updateId = Integer.parseInt(id);
		Message message = new Message();
		message.setText(text);
		message.setId(updateId);

		if (!isValid(text, errorMessages)) {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		new MessageService().update(message);
		response.sendRedirect("./");

	}

	private boolean isValid(String updateText, List<String> errorMessages) {
		if (StringUtils.isBlank(updateText)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < updateText.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
